package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

   HashMap<String, Double> books = new HashMap<String, Double>();

    public Quoter() {
           books.put("1", 10.0);
           books.put("2", 45.0);
           books.put("3", 20.0);
           books.put("4", 35.0);
           books.put("5", 50.0);
           books.put("others", 0.0);

     }

    public double getBookPrice(String isbn) {
        return books.get(isbn);
    }
}
